\subsection{}
Let $G : \{0,1\}^n \rightarrow \{0,1\}^{n+1}$ be a CS-PRG, then for any polynomial $p(n)$ we can construct a CS-PRG
$G' : \{0,1\}^n \rightarrow \{0,1\}^{p(n)}$.
We construct $G'$ with Algorithm~\ref{algo:n1pn}.
\begin{algorithm}[!htp]
\SetKwInOut{Input}{Input}\SetKwInOut{Output}{Output}
\Input{$s_0$ sampled uniformly in $\{0,1\}^n$.}
\Output{$\sigma$ cryptographically strong pseudorandom string of lenght $p(n)$.}
\For{$i \leftarrow 1$ \KwTo $p(n)$}{
$s_i | \sigma_i  \leftarrow G(s_{i-1})$\\
}
\KwRet{$\sigma_1 | \ldots | \sigma_{p(n)}$}
\caption{Algorithm to construct $G'$}\label{algo:n1pn}
\end{algorithm}
In words, at every iteration, we take the last bit of the output of $G$ to be the next bit in $\sigma$ and we use the first $n$ bits to re-seed $G$.

Suppose a PPT $T$ could distinguish between $\sigma$ and a truly random string of length $p(n)$ with success rate greater than any 
negligible function $\e$. Consider the following hybrid distribution denoted by $D_i$:
\begin{equation}
\underbrace{r_{1}| \ldots | r_{i}}_{i \text{ bits}} | \underbrace{\sigma_{1}|\ldots|\sigma_{p(n) - i}}_{p(n) - i \text{ bits}}
\end{equation}
Where $r_{1}| \ldots | r_{i}$ is a truly random string of length $i$.
We know that $T$ can distinguish between $D_0$ and $D_{p(n)}$:
\begin{equation}
|P_{x \leftarrow D_0}[T(x) = 1] - P_{x \leftarrow D_{p(n)}}[T(x) = 1] | > \e.
\end{equation}
Using a telescopic argument, we can re-write this expression as:
\begin{equation}
|\sum_{i = 1}^{p(n)} \left\{P_{x \leftarrow D_{i-1}}[T(x) = 1] -  P_{x \leftarrow D_i}[T(x) = 1]\right\} | > \e.
\end{equation}
and therefore there must be at least one $i \in {1,\ldots,p(n)}$ for which
\begin{equation}
|P_{x \leftarrow D_{i-1}}[T(x) = 1] - P_{x \leftarrow D_i}[T(x) = 1] | > \e/p(n).
\end{equation}
We can then use $T$ to tell apart a truly random string of length $n+1$ from the output of $G$ in polynomial time as follows.
Given a string $x$ of length $n+1$, we generate a string $\sigma$ using $G'$ and the first $n$ bits of $x$ as random seed. 
We then generate a hybrid string $y$ of length $p(n)$ by taking $i$ truly random bits, the last bit of $x$ and the first $p(n) - i$ bits of $\sigma$.

We know that $y$ is distributed according to $D_{i-1}$ or $D_{i}$ depending on whether or not $x$ was the output of $G$ or a truly random string and if we 
return the output of $T$ on $y$ we will be correct with non negligible advantage. 
If $T$ could tell $G'$ from random it could be used to tell $G$ from random which is a contradiction and $T$ cannot exist.

\subsection{}

Let $G : \{0,1\}^n \rightarrow \{0,1\}^{n+1}$ be a CS-PRG. Then $G' : \{0,1\}^{n^2} \rightarrow \{0,1\}^{n^2 + n}$ given by
$G'(x_1|\ldots|x_n) = G(x_1)|\ldots|G(x_n)$ is also a CS-PRG. Suppose, for the sake of contradiction, that $G'$ was not a CS-PRG, then there exists
a statistical test $T : \{0,1\}^{n^2 + n} \rightarrow \{0,1\}$ that can distinguish between the uniform distribution $U_{n^2 + n}$ and the
output distribution of $G'(U_n)$. We recall that $U_{n^2 + n}$ is identical to $U_{n+1} | \ldots | U_{n+1}$.

Let us call $D_i$ the hybrid distribution
\begin{equation}
\underbrace{G(U_n)|\ldots|G(U_n)}_{i \text{ blocks}}|\underbrace{U_{n+1}|\ldots|U_{n+1}}_{n-i \text{ blocks}}
\end{equation}

We have that $T$ can distinguish $D_0$ and $D_n$, more formally, for all negligible functions $\e$
\begin{equation}
|P_{x \leftarrow D_0}[T(x) = 1] - P_{x \leftarrow D_n}[T(x) = 1] | > \e.
\end{equation}
Using a telescopic argument, we can re-write this expression as:
\begin{equation}
|\sum_{i = 1}^n \left\{P_{x \leftarrow D_{i-1}}[T(x) = 1] -  P_{x \leftarrow D_i}[T(x) = 1]\right\} | > \e.
\end{equation}
and therefore there must be at least one $i \in {1,\ldots,n}$ for which
\begin{equation}
|P_{x \leftarrow D_{i-1}}[T(x) = 1] - P_{x \leftarrow D_i}[T(x) = 1] | > \e/n.
\end{equation}

We can use $T$ to construct a $T'$ that tells apart $G(U_n)$ and $U_{n+1}$ as follows, given an input string $x \in \{0,1\}^n$:
\begin{enumerate}
\item Sample $u_1,\ldots,u_{i-1}$ independently from $U_{n}$
\item Sample $v_{i+1},\ldots,v_{n}$ independently from $U_{n+1}$
\item Construct $y = G(u_1)|\ldots|G(u_{i-1})|x|v_{i+1}|\ldots|v_n \in \{0,1\}^{n^2 + n}$
\item Output $T(y)$
\end{enumerate}
we can see that $y$ is distributed according to $D_{i-1}$ if $x$ is distributed according to $U_{n+1}$. If, on the other hand, $x$ comes from $G(U_n)$ then 
$y$ will be distributed according to $D_i$ and therefore $T'$ has a success rate at least greater than any $\e/n$ which is a contradiction and the thesis follows.

\subsection{}
Given a pseudorandom function family $\mathcal{F} = \{ F_s : \{0,1\}^n \rightarrow \{0,1\}^n\}_{s\in\{0,1\}^n}$ we show that
$\mathcal{F}' = \{F_s' : \{0,1\}^{n^2} \rightarrow \{0,1\}^{n^2}\}_{s \in \{0,1\}^n}$
defined as $F'(x_1|\ldots|x_n) = F_s(x_1)|F_s(x_2)|\ldots|F_s(x_n)$ is not a pseudorandom function family.

In fact we can choose as input $X_0 = \underbrace{x|x|\ldots|x}_{n \text{ times}}$ for some \textbf{fixed} $x$.
We notice that the output of any $F_s'$ on $X_0$ will be structured. It will have some string in the first $n$ bits and then
the same string repeated $n-1$ times after that.

There are only $2^n$ truly random strings of length $n^2$ that have this property (one can only choose the first $n$ bits, the rest is fixed).
Recall that we fixed $x$ so in the counting number above we are using a fixed input and randomly sampled function.
A tester $T$ could then check if such a structure exists and output $1$ if it does and $0$ if it does not. Then we would have:
\begin{equation}
P[T(F(x)) = 1 | F(\cdot) \in \mathcal{F}'_s] = 1
\end{equation}
and
\begin{equation}
P[T(F(x)) = 1 | F(\cdot) \text{ function chosen at random in } \{0,1\}^{n^2} \rightarrow \{0,1\}^{n^2}] = \frac{2^n} { 2^{n^2}} = \frac{1}{2^n}
\end{equation}
It follows that the difference between the two probabilities is non-negligible and $T$ can tell if it's dealing with  a random function or with a member of the family
$\mathcal{F}'$ with non negligible advantage.

\subsection{}
Let $\mathcal{F} = \{ F_s : \{0,1\}^n \rightarrow \{0,1\}^n\}_{s\in\{0,1\}^n}$ be a pseudo-random function family. By applying the Feistel 
transformation twice to some $F_s$ we obtain a new family of functions $\mathcal{G} = \{G_{s_1,s_2} : \{0,1\}^{2n} \rightarrow \{0,1\}^{2n}\}_{s_1\in\{0,1\}^n, s_2\in\{0,1\}^n}$
given by:
\begin{equation}
G_{s_1,s_2}(L,R) = (F_{s_1}(R) \xor L, F_{s_2}(F_{s_1}(R) \xor L) \xor R)
\end{equation}
$\mathcal{G}$ is not a pseudorandom permutation family. In fact, for every two input that share the same $R$ block we have that any member of the family $G_{s1,s2}$ satisfies
\begin{align}
G_{s_1,s_2}(L_1,R) \xor G_{s_1,s_2}(L_2,R) = \\
(F_{s_1}(R) \xor L_1, F_{s_2}(F_{s_1}(R) \xor L_1) \xor R) \xor (F_{s_1}(R) \xor L_2, F_{s_2}(F_{s_1}(R) \xor L_2) \xor R) = \\
(L_1 \xor L_2,\qquad (F_{s_2}(F_{s_1}(R) \xor L_1) \xor R) \xor ( F_{s_2}(F_{s_1}(R) \xor L_2) \xor R)) = \\
(L_1 \xor L_2,\qquad (F_{s_2}(F_{s_1}(R) \xor L_1)\xor ( F_{s_2}(F_{s_1}(R) \xor L_2))).
\end{align}

The first half of the output is very structured: it is always $L_1 \xor L_1$. To build a test that tells
$G_{s_1,s_2}$ apart from random we can query the function with points s.t.  $L_1 = 0_n$ and $L_1 = 1_n$ and $R = 0_n$ and output
$1$ if the first half of the two outputs are such that their $\xor$ is zero (i.e. they are one the bitwise complement of the other) and $0$ otherwise.

The probability that a truly random permutation has the property above (i.e. the first $n$ bits of $f(0_n,0_n)$ are the bitwise complement of the first $n$ bits of
$f(1_n,0_n)$)
 is less than $1/(2^n -1)$ so we can tell $G_{s_1, s_2}$ from random with non negligible 
advantage.





















