We use the hint given in Goldreich's book. We are given a length preserving one-way function $f : \{0,1\}^n \rightarrow \{0,1\}^n$ which we use to construct
another length preserving function $g$ such that every individual bit of $g$'s output can be guessed with probability of success at least $3/4$ and $g$ is one way.

Consider the function $g(x,s) : \{0,1\}^{2n} \rightarrow \{0,1\}^{2n}$ defined as $g(x,s) = (f(x_s), x_{\bar{s}}, s)$ where $x_s$ denotes the 
concatenation of the $x_i$ bits of $x$ for which the $i$-th bit of $s$ is $1$.

We prove that $g(x,s)$ is one way. Suppose not and there is an algorithm $\A$ that inverts $g$ with non negligible advantage.
Then, given a string $y = f(x)$ of length $k$ for some $x$, we could sample a string $s$ so that exactly $k$ bits of $s$ are $1$. There are a non-negligible number of
such strings because we do not care about the length of $s$ (as long as it is polynomial in $k$ so that $s \in \{0,1\}^{p(k)}$ for some polynomial $p(\cdot)$). Therefore one is able to get one $s$ that is appropriate with non negligible 
probability in polynomial time by just sampling randomly in $\{0,1\}^{p(k)}$ and returning when one string with $k$ ones is found.
We can then sample a completely random string $z$ of length $p(k) - k$ and call $\A(y, z, s)$. The input $(y,z,s)$ is in the range of $\A$
because $y$ is $f(x)$ for some $x$ and it has no relation with $z$ (i.e. the two distributions $(f(x_s), x_{\bar{s}}, s)$ and
$(y, z, s)$ are identical for $x$ and $z$ sampled uniformly from the appropriate size strings sets).
We know that $\A(y,z,s) = (x',s')$ s.t. $f(x'_{s'}) = f(x)$ with non negligible advantage
and if we return $x'_{s'}$ as an inverse of $f$ we will be correct with non-negligible advantage. $\A$ therefore cannot exist  if $f$ is one way as we assumed 
and $g$ is one-way.

We can construct an algorithm $\A_i$ that guesses the $i$-th bit of $g(x,s)$ with probability at least $3/4$ which is a non-negligible advantage.
Given $y = (f(x_s),x_{\bar{s}},s) \in \{0,1\}^{2n}$, $\A_i$  will return $y_i$ if $i \geq n$ (note that $g$ reveals $s$ in its entirety) so:
\begin{equation}
P[\A_i(g(x,s)) = (x,s)_i]  = 1 \qquad \text{if } i \geq n.
\end{equation}

It will return $x_{\bar{s}_i}$ if $s_i$ is $0$ and
it will flip a coin otherwise.

We have, for $i \in \{1,\ldots,n\}$
\begin{align}
P[\A_i(g(x,s)) = (x,s)_i] &=&\\
P[\A_i(g(x,s)) = (x,s)_i | s_i = 0]\cdot P[s_i = 0] &+& \\
P[\A_i(g(x,s)) = (x,s)_i | s_i = 1] \cdot P[s_i = 1] &=&\\
\frac{1}{2}\cdot 1 + \frac{1}{2}\cdot\frac{1}{2} = \frac{3}{4}
\end{align}
