\subsection{}\label{p2a}
Let $h_r(x) = \langle r, s \rangle$, suppose for a given $r$ we have $h_r(x) = 0$, than for an arbitrary, but fixed, $k$ s.t. $r_k = 1$
(we need to modify the key-generation protocol
to avoid picking $r = 0^l$) we can write:
\begin{align}
\sum_{i=1}^l r_i x_i = 0 \mod 2\\
\Leftrightarrow r_k x_k + \sum_{i\neq k} r_i x_i = 0 \mod 2 \Leftrightarrow x_k = \left(-\sum_{i\neq k} r_i x_i\right)\mod 2\\
\end{align}
An analogous argument holds for $h_r(x) = 1$ and we can conclude that
$1/2$ of the elements $x \in \{0,1\}^l$ are such that $\langle r, x \rangle = 1$.
Given this fact, from a counting argument it follows that
 for all $x _1, x_2 \in \{0,1\}^l$ with $x_1 \neq x_2$ and $y_1, y_2 \in \{0,1\}$:
\begin{equation}
P[\langle r, x_1 \rangle = y_1 \wedge \langle r, x_2 \rangle = y_2 | r \leftarrow U^l] = \frac{1}{2}\cdot\frac{1}{2}=\frac{1}{4}
\end{equation}
where the probability is taken over $x_1, x_2, y_1$ and $y_2$. Therefore, by definition, $h_r(\cdot)$ is a pairwise independent hash 
function family. 

This implies that for all $\alpha \in \{0,1\}^{m}$ and all functions
$f(s): \{0,1\}^l \rightarrow \{0,1\}^{m}$, the distribution of $(r,\langle r,s \rangle)$, conditioned on $f(s) = \alpha$ and 
the distribution of $(r,\sigma)$ for a random bit $\sigma$ are $2\sqrt{\frac{2}{|f^{-1}(\alpha)|}}$-close in statistical distance,
where $|f^{-1}(\alpha)|$ denotes the size of the subspace of $\{0,1\}^{m}$ that maps to $\alpha$ under $f(\cdot)$.
\begin{claim}\label{cl:p2a}
For any computationally unbounded $\A$ and any function $f(s): \{0,1\}^l \rightarrow \{0,1\}^{m}$ we have that:
\begin{equation}
P_{r,s}[ \A(r,f(s)) = \langle r,s \rangle] \leq \frac{1}{2} + 2\sqrt{2} \cdot 2^{-\frac{l}{2}+m}
%\frac{1}{2}\sum_{\alpha \in \{0,1\}^{m}} \frac{|f^{-1}(\alpha)|}{2^{l}} + 
%\sum_{\alpha \in \{0,1\}^{m}} \frac{2\sqrt{2|f^{-1}(\alpha)|}}{2^{l}}
\end{equation}
\end{claim}
\begin{proof_of}{Claim~\ref{cl:p2a}}
Let $\sigma$ be a random bit, then:
\begin{align}
P_{r,s} [\A(r,f(s)) = \langle r,s \rangle] =\\ 
\sum_{\alpha \in \{0,1\}^{m}} P_s[f(s) = \alpha] \cdot P_{r,s}[\A(r,\alpha) = \langle r,s \rangle | f(s) = \alpha] = \\
\sum_{\alpha \in \{0,1\}^{m}} \frac{|f^{-1}(\alpha)|}{2^{l}} \cdot P_{r,s}[\A(r,\alpha) = \langle r,s \rangle | f(s) = \alpha]\leq \\
\sum_{\alpha \in \{0,1\}^{m}} \frac{|f^{-1}(\alpha)|}{2^{l}} \cdot \left( P_{r,\sigma}[\A(r,\alpha) = \sigma]  +  2\sqrt{\frac{2}{|f^{-1}(\alpha)|}} \right) \leq \\
\frac{1}{2}\sum_{\alpha \in \{0,1\}^{m}} \frac{|f^{-1}(\alpha)|}{2^{l}} + 
\sum_{\alpha \in \{0,1\}^{m}} \frac{2\sqrt{2|f^{-1}(\alpha)|}}{2^{l}} \leq \frac{1}{2} + 2\sqrt{2} \cdot 2^{-\frac{l}{2}+m}.
\end{align}
\end{proof_of}

We can apply Claim~\ref{cl:p2a} to the scheme with incorrectly formed public keys by viewing
$f(s) = \text{Eval}_{\xor}(s)$  to highlight that it does not depend on $r$ for the incorrectly formed public key. In this case 
we have $m = B(k)$.

Then we can write:
\begin{align}
Pr[\A(r,\text{Eval}_{\xor}(s)) = \langle r, s \rangle| &\\
{}&(s,r) \leftarrow \{0,1\}^l, sk \leftarrow G(1^k),\\
{}&pk = (r, \underbrace{\text{Enc}(sk,0),\ldots,\text{Enc}(sk,0)}_{l - \text{instances}}] \\ \leq \frac{1}{2} + 2\sqrt{2}\cdot 2 ^{-\frac{l}{2} + B(k)}
\end{align}
Since we have that $l = 4B(k)$, the right hand side of the inequality above can be written as $\frac{1}{2} + \e(k)$ for some
negligible function $\e(\cdot)$.
\subsection{}
Suppose that there was a PPT adversary $\B$ that can predict the value of a single bit message $\sigma$
based on its encryption under the scheme $(\text{Gen}', \text{Enc}', \text{Dec}')$. Then we have that:
\begin{align}
Pr[\B(r,\text{Enc}'(pk, \sigma)) = \langle r, s \rangle| &\\
{}&(s,r) \leftarrow \{0,1\}^l, sk \leftarrow G(1^k),\\
{}&pk = (r, \text{Enc}(sk,r_1),\ldots,\text{Enc}(sk,r_l)] \\ > \frac{1}{2} + \frac{1}{p(k)}
\end{align}
for some polynomial $p(\cdot)$. We can see that $\B$ can be used to construct a distinguisher that tells apart encryptions
of a random $r$ from encryptions of $0^l$. On input $r \leftarrow \{0,1\}^l$ and $Y$, which is either obtained
by encrypting the bits of $r$ or by encrypting $0^l$, the distinguisher choses $s \leftarrow \{0,1\}^l$ and uses 
$\sigma' = \B(r, Y)$. It outputs $1$ if $\sigma' = \langle r, s\rangle$ and $0$ otherwise. According to the performance of $\B$ and
the inequality discussed in Part~\ref{p2a} the distinguisher has a non negligible advantage. This distinguisher can in turn 
be used to win the computational indistinguishability challenge for many messages of the original encryption scheme
by choosing $m_0^1, \ldots, m_0^l = 0^l$ and $m_1^1,\ldots, m_1^l = r_{1,\ldots,l}$ . Therefore $\B$ cannot exist and the scheme is secure.



















