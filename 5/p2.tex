\subsection{}\label{p2a}
The decrypt algorithm $\text{Dec}(sk, (c,r), pk)$ is as follows:
\begin{enumerate}
\item Run $sk_r \leftarrow \text{Extract}(PP\leftarrow pk, ID \leftarrow r,msk \leftarrow sk)$
\item Compute $m' \leftarrow \text{Decrypt}(pk, sk_r, c)$
\end{enumerate}

This decryption scheme is correct as long as the original IBE is correct because we are 
decrypting using the appropriate secret key. More precisely, as long as for the original IBE scheme
we have that $\text{Decrypt}(PP, \text{Extract}(PP, r, msk), \text{Encrypt}(PP, r, m)) = m$,
the decryption algorithm proposed above is correct.

\subsection{}\label{p2b}
Suppose the scheme described in Part~\ref{p2a} is not ``lunchtime'' secure and there is a
PPT algorithm $\A$ 
that wins the CCA--1 game with non negligible advantage. If that is the case, we can use $\A$ 
to construct a PPT algorithm $\B$ that wins the IBE game with exactly the same probability
with which $\A$ wins the CCA--1 game.

Here is how $\B$ is constructed.

\begin{enumerate}
\item Whenever $\A$ calls $\text{Enc}(pk, m)$:
	\begin{enumerate}
	\item Generate a random $r \leftarrow \{0,1\}^n$
	\item Compute $c \leftarrow \text{Encrypt}(pk, r,m)$.
	\item Give $(c,r)$ to $\A$
	\end{enumerate}
\item Whenever $\A$ calls $\text{Dec}(sk, (c,r), pk)$:
	\begin{enumerate}
	\item Ask for $sk_r \leftarrow \text{Extract}(pk, r, sk)$ to the IBE challenger.
	\item Compute $m' \leftarrow \text{Decrypt}(pk, sk_r, c)$.
	\item Give $m'$ to $\A$.
	\end{enumerate}
\item When $\A$ outputs $m_0$ and $m_1$, output the same messages with a random ID 
$r \leftarrow \{0,1\}^n$
\item When the IBE challenger returns $c$:
	\begin{enumerate}
	\item Give $(c,r)$ to $\A$
	\end{enumerate}
\item Keep simulating $\text{Enc}(pk, m)$ as before.
\item Output whatever $\A$ outputs.			
\end{enumerate}
$\B$ succeeds whenever $\A$ succeeds and fails whenever $\A$ fails. This is because
the view of $\A$ when used by $\B$ is the same as the view of $\A$ when playing the actual
CCA--1 game. More formally, the distributions of $(c, r)$ as simulated by $\A$ and
as produced by a CCA--1 challenger for the scheme in Part~\ref{p2a} are identical.

\subsection{}
Given a strong one--time signature scheme ($\text{Gen}_S, \text{Sign}, \text{Verify})$
we can modify the encryption scheme presented in Part~\ref{p2b}
by generating a signature/verification key pair which we use to sign the cipher text. 
The ID we give to the IBE scheme for encrypting will be the verification key.
Here is the modified encryption scheme:

\begin{itemize}
\item $\text{Gen}(1^n$): Same as in Part~\ref{p2b}.
\item $\text{Enc}(pk, m)$:
	\begin{itemize}
	\item $(sk, vk) \leftarrow \text{Gen}_S(1^n)$ (sig/ver key pair)
	\item $c \leftarrow \text{Encrypt}(pk, vk, m)$
	\item $\sigma \leftarrow \text{Sign}(sk, c)$
	\item Output $(vk, c, \sigma)$.
	\end{itemize}
\item $\text{Dec}(sk, (vk, c, \sigma), pk)$:
	\begin{itemize}
	\item if $\text{Verify}(vk, c, \sigma) == 1$:
		\begin{itemize}
		\item Run $sk_{vk} \leftarrow \text{Extract}(pk, vk, sk)$
		\item Compute $m' \leftarrow \text{Decrypt}(pk, sk_{vk}, c)$.
		\item Output $m'$.
		\end{itemize}
	\item else:
		\begin{itemize}
		\item Output $\perp$
		\end{itemize}
	\end{itemize}
\end{itemize}

This scheme is CCA--2 secure. Suppose there is an adversary $\A$ that wins the CCA--2 game 
with more than negligible advantage. Then we can construct $\A'$ to break the semantic security of
the IBE scheme. The algorithm $\A'$ is very similar to the algorithm constructed in Part~\ref{p2b}.

Here is how to construct $\A'$:
\begin{enumerate}
\item Whenever $\A$ calls $\text{Enc}(pk, m)$:
	\begin{enumerate}
	\item $(sk, vk) \leftarrow \text{Gen}_S(1^n)$ (sig/ver key pair)
	\item $c \leftarrow \text{Encrypt}(pk, vk, m)$
	\item $\sigma \leftarrow \text{Sign}(sk, c)$
	\item Give $(vk, c, \sigma)$ to $\A$
	\end{enumerate}
\item Whenever $\A$ calls $\text{Dec}(sk, (vk, c, \sigma), pk)$:
	\begin{enumerate}
	\item Ask for $sk_{vk} \leftarrow \text{Extract}(pk, vk, sk)$ to the IBE challenger.
	\item Compute $m' \leftarrow \text{Decrypt}(pk, sk_{vk}, c)$.
	\item Give $m'$ to $\A$.
	\end{enumerate}
\item When $\A$ outputs $m_0$ and $m_1$
	\begin{enumerate}
	\item Run $(sk^*, vk^*) \leftarrow \text{Gen}_S(1^n)$.
	\item Give $m_0$ and $m_1$ to the challenger along with ID $vk$.
	\end{enumerate}
\item When the IBE challenger returns $c^*$:
	\begin{enumerate}
	\item Compute $\sigma^* \leftarrow \text{Sign}(sk^*, c^*)$
	\item Give $(vk^*,c,  \sigma^*)$ to $\A$
	\end{enumerate}
\item Keep simulating the encryption queries as before.
\item Whenever $\A$ queries for a decryption $\text{Dec}(sk, (vk, c, \sigma), pk)$ 
with $(vk, c, \sigma) \neq (vk^*, c^*, \sigma^*)$:
	\begin{enumerate}
	\item If $\text{Verify}(vk, c, \sigma) == 0$:
		\begin{enumerate}
		\item Return $\perp$ to $\A$.
		\end{enumerate}
	\item Else:
		\begin{enumerate}
		\item if $vk == vk^*$ stop executing and output a random bit.
		\item else: compute the result of the decryption as before.
		\end{enumerate}
	\end{enumerate}
\end{enumerate}

The view of $\A$ is indistinguishable from the real CCA--2 game except for queries of the form
$(vk^*, c, \sigma)$ with $(c, \sigma) \neq (c^*, \sigma^*)$,
but the probabilities of such queries is negligible or the original signature
scheme would not be one time secure. So we can conclude that the probability of success of 
$\A'$ is equal to the probability of success of $\A$ except for a negligible term.
So if the probability of success of $\A$ was non negligible, so would be that of $\A'$. This shows
that $\A$ cannot exist and the scheme is CCA--2 secure.


