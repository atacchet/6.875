The following proofs are adapted from: \textit{How to Leak a Secret: 
Theory and Applications of Ring Signatures} Rivest, Shamir and Tauman 2006.
\subsection{}
The identity of the signer is protected unconditionally. The ring equation:
\begin{equation}
E_{h(m)}(E+{h(m)}(g_1(x_1) \xor v) \xor g_2(x_2)) = v
\end{equation}
has exactly $2^n$ for solutions in $x_{k + 1 \mod 2}$ for any $x_k$ and $v$ for $k \in \{1,2\}$.
 All of these solutions
can be chosen with equal probability regardless of the signer's identity. The distribution of
$(x_1, x_2, v)$ carries no information on whether $x_1$ or $x_2$ was chosen at random or computed
by the signer.
Therefore, even an all powerful adversary cannot recover the identity of the signer from the signature.

\subsection{}\label{p5b}
We show that for any adversary $\A$ that has non negligible advantage in the unforgeablity
game we can construct $\A'$ that does not query the ring signing challenger but only
uses $h$, $E_k$ and $E_k^{-1}$ (and $g_1$ and $g_2$) and still wins the unforgeablity game with
more than negligible advantage.

Here is how we construct $\A'$:
\begin{enumerate}
\item When $\A$ queries the ring signing oracle with a new message $m$:
	\begin{enumerate}
	\item Respond with a random tuple $(x_1, x_2, v)$.
	\item Set $z_0 = z_2 = v$.
	\item Choose a value $z_1$ at random. 
	\item These last two instructions are such that the ring
	equation is satisfied for the queries in the following cases, that are related to previously queried
	$m$'s.
	\end{enumerate}

\item When $\A$ queries its encryption oracle with $E_{h(m)}(v \xor g_1(x_1))$, respond $z_1$.
\item When $\A$ queries its encryption oracle with $E_{h(m)}(z_1 \xor g_2(x_2))$ respond $v$.
\item When $\A$ queries its decryption oracle with $E_{h(m)}^{-1}(z_1)$ respond $v \xor g_1(x_1)$.
\item When $\A$ queries its decryption oracle with $E_{h(m)}^{-1}(v)$ respond $z_1 \xor g_2(x_2)$.

\item When $\A$ produces its output $(m, i)$ where $i$ is the identity of the signer $i \in \{1,2\}$.
Get the signature for $m$ as $(x_1, x_2, v)$ from the challenger and respond to $\A$.
\item Keep responding to $\A$'s queries as above
\item Output whatever $\A$ outputs.
\end{enumerate}

We have that $\A'$ succeeds with roughly the same probability as $\A$. 
The main reason is that $\A$ cannot ask its oracle for encryptions (decryptions) of the form
$E_{h(m)}(z_i \xor g_{i+1 \mod 2}(x_{i+1 \mod 2}))$ ($E_{h(m)}^{-1}(z_{i+1 \mod 2})$) before providing
$m$ to $\A'$ except with negligible probability because $\A'$ chooses its $z_j$'s for $m$ randomly
after $\A$ provides $m$ and therefore $\A$ cannot guess them in advance.

Therefore, except for a negligible term, the view of $\A$ is identical whether it interacts with $\A'$
or the actual challenger.

\subsection{}\label{p5c}
We assume the existence of $\A'$ from Part~\ref{p5b} and we build a PPT $\B$ that uses $\A'$ as a
black box and on input $y$ finds one of $g_1^{-1}(y)$, $g_2^{-1}(y)$.
Here is how to construct $\B$.
We note that $\A'$ must call its oracle for $h$ with the message it is actually going to forge or the 
probability of satisfying the ring equation becomes negligible.
$\A'$ runs in polynomial time and we assume, wlog, that it makes all the queries for the
$h$'s then all the queries to $E_k$ and to $E^{-1}_k$. Since $\A'$ is a PPT 
algorithm it can query $h$ on, at most, 
a polynomial number of messages and therefore one can guess, with non 
negligible advantage, which message $m^*$ $\A'$ is going to forge by just picking a $j$ at random.
$\B$ does exactly that and sets $k = h(m^*)$ where $m^*$ is the $j$--th message for which $\A'$ 
queries $h$. All queries to $h$ are answered randomly unless they are duplicates in which case 
they are answered consistently.

Whenever $\A'$ queries $E_k$ and $E_k^{-1}$, $\B$ first checks if $k = h(m^*)$.
If that is not the case then $\B$ answers $\A'$ queries with a random strings unless the same query, or 
the corresponding
opposite query, was already answered, in which case it is answered with the predetermined value.

So we only need to figure out how to answer $E_k$ and $E^{-1}_k$ when $k = h(m^*)$.
Suppose $\A'$ performs $t$ queries to and $E_k^{-1}$ obtaining a disjoint set of pairs
$\{(w_i, z_i)\}_{i=1}^t$ where $z_i = E_k(w_i)$. 

With high probability $\A'$ will query $z_{i_1}$ and $z_{i_2}$ s.t. 
$z_{i_j} = v$ and 
$E_k(z_{i_{j-1}} \xor g_j(x_j)) = z_{i_j}$ for $j \in \{1,2\}$ (all indexes are $\mod 2$).

We can assume wlog that for all $j \in \{1,2\}$ $E_k$ is queried in the clockwise direction 
for $w_{i_j}$. A similar argument to the one presented below holds if $E_k^{-1}$ is called 
in the counterclockwise direction for all $z_{i_j}$. It does not make sense to use both strategies 
because the information one gains is redundant (i.e. we query for the decryption of ciphers 
of which we already know the cleartext because we previously queried for the 
encryption of said messages) so we can ignore this case.

The idea is to force $\A'$ to invert $g$ for us by creating a gap in the ring that leads to a valid signature.

The structure of the ring implies that there is a $j \in \{1,2\}$ such that $w_{i_j}$ was queried before
$w_{i_{j-1}}$ (all indexes are $\mod 2$). 
Suppose $w_{i_2}$ was queried before $w_{i_1}$, then $\B$ will guess
which of all queries $\{w_i\}_{i=1}^t$ corresponds to $w_{i_{j-1}}$ (he has a non negligible proability
of guessing right because of $\A'$ is a PPT algorithm). Then $\B$ will reply to the query for 
$E_k(w_{i_{j-1}})$ with $w_{i_j} \xor y$. All other queries are answered randomly
but consistently as before.  The forgery produced by $\A'$, $(x_1, x_2, v)$,
if valid, which happens with non negligible advantage, must satisfy the ring equation.
We notice that the $y$ that $\B$ ``slipped'' in the gap, occupies the place of either $g_1(x_1)$ or
$g_2(x_2)$ and therefore we can propose $x_i \in g_i^{-1}(y)$ as inverse value.

The view of $\A'$ when used by $\B$ is statistically indistinguishable from its forgery challenge,
so $\A'$ will be correct with non negligible advantage. $\B$ must also correctly guess $j$ 
but it has a non negligible advantage in doing so and therefore its probability of success is non 
negligible.

%Given the ring equation the XOR of the values in the gap
%is the desired inverse because $\A'$ must satisfy the ring equation so we have:
%\begin{flalign}
%E_k(w_{i_{j-1}}) = w_{i_j} \xor y\\
%E_k(E_k(w_{i_{j-i}}) \xor g_j(x_j)) = E_k(w_{i_{j}} \xor y \xor g_j(x_j)) = E_k(w_{i_j} \xor y \xor y)\\
%=  E_k(w_{i_j}) = w_{i_{j+1}} \xor y = v.
%\end{flalign}
%For this equality to hold we must have that $(w_{i_{j-1}}) \xor (w_{i_{j+1}}) \in g_j^{-1}(y)$.

\subsection{}
Given $\B$ from Part~\ref{p5c} we can construct an algorithm $\B'$ that takes as input a particular
$g$ and a $y$ and outputs an inverse of $y$ under $g$ with non negligible probability.
$\B'$ can choose a random $j \in \{1,2\}$
and sets $g_j = g$ and sets the other trapdoor permutation $g_i$ for $i \in \{1,2\}$ and $i \neq j$ to a 
random trapdoor permutation. Agorithm $\B$ will output with non negligible advantage 
an inverse of $y$ under one of $g_j, g_i$ and therefore by just copying $\B$'s output, $\B'$ will be 
right 
with non negligible advantage.