The following is adapted from \textit{Proofs that yield nothing but their validity or all languages in NP 
have zero-knowledge proof systems}, O. Goldreich, S. Micali and A. Wigderson - 1991.

\subsection{}\label{p2a}
Protocol between Prover $P$ and verifier $V$ for membership in the graph non-isomorphism
language. Let $(G_0, G_1)$ be two graphs with $k$ vertexes and let $n$ be polynomial in $k$.

\begin{enumerate}
\item The verifier chooses a random bit $\alpha$ and a random permutation $\pi$ and constructs
$H = \pi G_{\alpha}$. The verifier also constructs $n$ pairs of graphs such that
in each pair one graph is isomorphic to $G_0$ and the other is homomorphic to $G_1$.
She stores the two graphs in each pair in random order. She chooses $n$ 
random bits $\gamma_i$ for $i = 1,\ldots,n$ and $n$ pairs of permutations $\tau_{i,0}$ and
$\tau_{i,1}$. She can now compute $T_{i,j} = \tau_{i,j}G_{j + \gamma_i \mod 2}$.

The verifier then sends $H$ and the pairs $(T_{1,0}, T_{1,1}), \ldots, (T_{n,0}, T_{n,1})$ to the 
prover.

\item The prover choses a string $I \in \{0,1\}^{n}$ uniformly at random and sends it to the verifier.

\item The verifier checks that $I$ is indeed a string from $\{0,1\}^{n}$ and if that is not the case, she
rejects the proof and stops executing. Otherwise, the verifier replies with all the tuples
$(\gamma_i, \tau_{i,0}, \tau_{i,1})$ for all $i$'s for which the $i$-th bit of $I$ is $1$, and all the pairs 
$(\alpha + \gamma_k \mod 2, \tau_{k, (\alpha + \gamma_k \mod 2)} \pi^{-1})$ for all $k$'s for which
the $k$-th bit of $I$ is $0$. She is sending an isomorphism between the input graph and the $i$-th pair
and an isomorphism between $H$ and the $k$-th pair, so proving that the $i$-th pair
was constructed as planned and that if the $k$-th pair was constructed as planned then also $H$
was, for all $i$ and $k$ as above.

\item The prover now checks that the messages produced by the verifier are legitimate. In particular he
makes sure that $\tau_{i,j}$ is an isomorphism between $T_{i,j}$ and $G_{(j + \gamma_i \mod 2)}$,
for all $i$'s such that the $i$-th bit of $I$ is $1$ and $j \in\{0,1\}$ and that 
$\tau_{k, (\alpha + \gamma_k \mod 2)} \pi^{-1}$ is an isomorphism between 
$T_{k, (\alpha + \gamma_k \mod 2)}$ and $H$ for all $k$'s such that the $k$-th bit of $I$ is $0$.
If the prover finds any condition to be violated he stops executing. Otherwise he produces 
$\beta \in \{0,1\}$ such that $H$ is isomorphic to $G_{\beta}$. If he can't produce one, he sets $\beta$ to
zero.	

\item Finally the verifier checks whether or not $\alpha = \beta$, if that is the case, she accepts the proof
otherwise she rejects (and if this was one of many rounds she also stops executing all other rounds).

\end{enumerate}

The protocol is obviously complete: if $G_0$ is not
isomorphic to $G_1$ and both $P$ and $V$ follow the protocol described above, then, without failure,
$V$ will find that $\alpha = \beta$. If, on the other hand, $G_0$ is isomorphic to $G_1$, then
all the permutations and graphs produced by the verifier are isomorphic copies of $G_0$ and $G_1$
which are random and convey no information about which $\alpha$ was chosen. Therefore with
probability at least $1/2$, in this case, the verifier will find that $\alpha \neq \beta$. This fact
holds regardless of the verifier following or not the protocol.

\subsection{}
We will show that the protocol presented in Part~\ref{p2a} is statistical zero-knowledge.

For every verifier $V^*$ we will construct a simulator $MV^*$, that uses $V^*$ as a subroutine
and we will show that $MV^*(x)$ and  $\langle P, V^*\rangle(x)$ where $\langle P, V^* \rangle$ denotes
$\text{View}_{(P, V^*)}$ are statistically close.

First we suppose that $V^*$ is strictly polynomial time and we let $q$ be an upperbound on its running
time on input graphs $G_0$ and $G_1$. We then sample $r \in \{0,1\}^q$ uniformly at random and we use it
as the randomness for $V^*$.
Every time $V^*$ requires randomness, it will read a block from $r$. 

Here is how $MV^*$ works:

\begin{enumerate}
\item Start $V^*$ on $G_0$ and $G_1$ with $r$ as random generator. $V^*$
will output $H$ and a sequence of $(T_{i,0},T_{i,1})$ for $i \in 1,\ldots,n$ ($n$ is the same as in Part~\ref{p2a}). All these 
are appended to the log.

\item $MV^*$ produces a random string $I \in \{0,1\}^{n}$, gives it to $V^*$ and writes
it on its log.

\item $V^*$ produces all the tuples
$(\gamma_i, \tau_{i,0}, \tau_{i,1})$ for all $i$'s for which the $i$-th bit of $I$ is $1$, and all the pairs 
$(\alpha + \gamma_k \mod 2, \tau_{k, (\alpha + \gamma_k \mod 2)} \pi^{-1})$. $MV^*$ appends
these tuples to the log and then performs
all the checks that a honest verifier would perform. If any fails it stops and outputs
its log. Otherwise $MV^*$ goes on.

\item Now $MV^*$ sets out to find an isomorphism between $H$ and $G_{\alpha}$ by rewinding $V^*$.
It repeats the following steps at most $2^{n}$ times; if it finds a good $K$ (see later) it goes to step (5). If no good $K$ (see later) is found after $2^{n}$ rewinding then $MV^*$ aborts.
	\begin{enumerate}
	\item $MV^*$ samples a new string $K \in \{0,1\}^{n}$, it restarts $V^*$ on the same two
	graphs $G_0$ and $G_1$ and the same $r$ (rewinded).
	
	\item $MV^*$ sends $K$ as its first message (step 2 of the protocol).
	
	\item $V^*$ produces $(\delta_i, \sigma_{i,0}, \sigma_{i,1})$ and
	$(\alpha + \delta_k \mod 2, \sigma_{k, (\alpha + \delta_k \mod 2)} \pi^{-1})$ for all $i$'s and 
	such that the $i$-th bit of $K$ is $1$ and all $k$'s such that the $k$-th bit of $K$ is $0$.
	
	\item Now we have that for all $j$'s such that the $j$-th bit of $I$ is $1$ and the $j$-th bit of $K$ is
	$0$, if $\sigma_{j, (\alpha + \delta_j \mod 2)} \pi^{-1}$ is an isomorphism between 
	$T_{j, \alpha + \delta_j \mod  2}$ and $H$, then we have found an isomorphism between $H$ and 
	$G_{\alpha}$. Similarly for all the $j$'s such that $j$-th bit of $I$ is $0$ and the $j$-th bit of $K$ is
	$1$ $MV^*$ can build an isomorphism between $H$ and $G_{\alpha}$. 
	If $MV^*$ did not find an isomorphism, it goes back to step (a).
	\end{enumerate}

\item Now for whatever $\beta \in \{0,1\}$ such that we have an automorphism between $G_{\beta}$ and $H$ $MV^*$ appends $\beta$ to its log.
\end{enumerate}

Suppose the verifier $V^*$ had a non negligible probability $\e$ of getting to step (5) of the original 
protocol, then it has the same probability of getting to step (5) when it interacts with $MV^*$, except
for a negligible term. The probability of getting to step (5) is the probability of getting a good answer
from $V^*$ on some $K \neq I$ (either way the bits happen to be we can always get the isomorphism) and the 
difference between this probability and $\e$ can be at most $2^{-n}$.

Now it remains to show that the distributions $\langle MV^*,V^* \rangle(G_0, G_1)$ and 
$\langle P, V^*\rangle(G_0, G_1)$ are close and that 
$MV^*$ terminates in expected polynomial time.

\begin{claim}\label{mv}
$MV^*$ terminates in expected polynomial time.
\end{claim}
\begin{proof_of}{Claim~\ref{mv}}
For a fixed string $r$, input graphs $G_0$ and $G_1$, and $V^*$, let's call a string $I \in \zo^{n}$ 
good if $V^*$ answers properly on $I$ and bad otherwise. We need to compute
how many times $MV^*$ will rewind $V^*$ in step (4). Let $g_r$ be the number of good strings.

If $g_r \geq 2$ then we have to consider the probability that the string chosen in step (2) is good 
and at the same time the string $K$ chosen in step 4.(a) is also good for $K \neq I$. 
The expected number of times $V^*$ is rewinded is (including the first call in step (1)):
\begin{equation}
1 + \frac{g_r}{2^{n}} \cdot \left(\frac{g_r -1}{2^{n}}\right)^{-1} \leq 3
\end{equation}

If $g_r = 1$ then the probability of getting the right string $I$ in step (2) is negligible.
If $MV^*$ is that unlucky, then it will rewind $V^*$ $2^{n}$ times without finding a good $K$, but this only
happens with negligible probability.
$MV^*$ is only expected polynomial time.
If any other $I$ in step (2) is chosen then $V^*$ will be rewinded only once. 

Lastly, if $g_r = 0$ then the string $I$ from step
(2) can never be good and $V^*$ will only be rewinded once. We can conclude that $MV^*$ is
expected polynomial time if $V^*$ runs in polynomial time.

\end{proof_of}

\begin{claim}\label{zn}
The probability distribution of $\langle MV^*, V^* \rangle(G_0,G_1)$ is the same as the probability
distribution of $\langle P, V^*\rangle(G_0, G_1)$ except for a negligible term.

\end{claim}
\begin{proof_of}{Claim~\ref{zn}}
The two views are identical except for the case in which $MV^*$ fails to pass step (4) but the probability
of this event is negligible as discussed above. %This probability is $\approx (t/2^n) e^{-(t-1)}$.
\end{proof_of}

We proved that the protocol is sound, complete and that there is a simulator that produces, in 
polynomial time a view that is statistically indistinguishable from the interaction with the real prover.
We can conclude that the protocol is statistical zero-knowledge.

\subsection{}
The protocol described in Part~\ref{p2a} is zero-knowledge even if executed sequentially $m$ times.
The probability that a dishonest prover cheats the verifier is, in this case, $2^{-m}$.
The machine $MV^*$ can also run $m$ times appending its communications with $V^*$ to its log.
This log will be statistically indistinguishable from the view of $V^*$ interacting with a prover $m$ 
times. As long as $m$ is polynomial in the size of the graphs, the running time of this new simulator will
also be expected polynomial in the same quantity.

\subsection{}

The protocol works also in parallel because it only requires $MV^*$ to find a good $I$ and a good
$K$ per copy. 
The verifier in this case picks random bits $\alpha_1, \ldots, \alpha_m$, and permutations $\pi_1, \ldots, \pi_m$.
The provers picks random strings $I_1, \ldots, I_m$ each in $\zo^n$ and only then they start the parallel rounds.
(note that all communications happen in batch, i.e. the verifier sends all the $\alpha$'s and $H$'s at once and so
does the prover for the $I$'s).
If $m$ is polynomial in the size of the graphs (so $m \leftarrow O(\log(k))$ is fine) the protocol remains zero knowledge.
The soundness error in this case is $2^{-m}$ like in the sequential case.

The proof that the protocol is zero-knowledge is also unaltered for the most part, the only difference is that 
the communications happen in batch, that is the simulator will prepare all the $I$'s strings and send them to the
verifier and then it will prepare all the $K$'s strings (one for each copy) all together to the verifier when it rewinds it in
step (4).

The rest of the analysis is very similar to what we described above\footnote{See also textit{Proofs that yield nothing but their 
validity or all languages in NP 
have zero-knowledge proof systems}, O. Goldreich, S. Micali and A. Wigderson - 1991. Remark 12}.

\subsection{}
In the previous question we stated that for $m \leftarrow O(poly(k))$ the protocol remains zero knowledge, therefore 
$m \leftarrow O(k)$ is fine. This is in contrast with the graph isomorphism protocol which breaks in this 
case\footnote{See also \textit{Foundations of Cryptography}, O. Goldreich, Section 4.3.2}.

