\subsection{}\label{p4a}

We will start by describing the notion of garbled circuits that we use in this problem. Given a circuit $C$ and an input $x$,
we can compute a garbled version of the circuit $\hat{C}$ and of the input $\hat{x}$. Someone who has access to $\hat{C}$ and
$\hat{x}$ can compute the quantity $C(x)$ (note that not garbled versions are used here) but not learn anything about
$x$ or about $C$. 
%In particular, for example, it should not be able to compute $C$ on any other input.
To make the notation a little easier we will imagine that $C$ has $n$ input pins and that $x \in \zo^n$. Then garbling 
amounts to randomly permuting all the truth tables of the gates in $C$ and associate to each input pin a pair of keys
(or labels) one for each possible value of the input at that pin. The garbling of $x$ then is of the form 
$\hat{x} = k_{1,x_1},\ldots,k_{n,x_n}$. Feeding $ k_{1,x_1},\ldots,k_{n,x_n}$ allows us to compute $C(x)$.

The security requirement for garbled circuits is as follows: there exists a simulator\\
$Sim(C(x), \text{ description of }  C)$ such that, for any circuit $C$, the following distributions are computationally 
indistinguishable:
\begin{enumerate}
\item $(\hat{C}, k_{i,x_i})$ with $(\hat{C},k)$ being the garbled version of $C$.
\item $Sim(C(x), \text{ description of }  C)$.
\end{enumerate}

We assume that there exists a universal circuit $U$ that given as input the description of a function $f$ and an input
$x$ is able to produce $f(x)$, that is $U(f,x) = f(x)$.

We propose the following scheme for one-query functional encryption:

\begin{itemize}
\item $FE.Setup(1^n) \rightarrow (mpk, msk)$
	\begin{itemize}
	\item Generate $2m$ key pairs using the public key encryption scheme: $(pk_{i,b}, sk_{i,b}) \leftarrow Gen(1^k)$
		with $i \in \{1,\ldots, m\}$ and $b \in \zo$.
	\item Set the master secret key as: $msk = (sk_{1,0}, sk_{1,1},\ldots,sk_{m,0}, sk_{m,1})$
	\item Set the master public key as: $psk = (pk_{1,0}, pk_{1,1},\ldots,pk_{m,0}, pk_{m,1})$
	\end{itemize}
\item $FE.KeyGen(msk, f) \rightarrow sk_f$
	\begin{itemize}
	\item We denote by $f_1,\ldots,f_m$ the $m$ bits description of $f$.
	\item Output $sk_f = (sk_{1,f_1},\ldots,sk_{m,f_m})$.
	\end{itemize}
\item $FE.Enc(mpk, x)$
	\begin{itemize}
	\item Generate a garbling of the circuit $U(\cdot, x)$ where the input $x$ is hard-coded. We will denote this by $\hat{U}_x$.
	\item Let $(k_{1,0},k_{1,1},\ldots,k_{m,0},k_{m,1})$ denote the input labels to the circuit $\hat{U}_x$ (these are just
		the pins for the description of $f$, because the input $x$ is hard coded).
	\item Compute $c_{i,b}  = Enc(pk_{i,b}, k_{i,b})$ for $i \in \{1,\ldots,m\}$ and $b \in \zo$.
	\item Output all the $c_{i,b}$'s and $\hat{U}_x$.
	\end{itemize}
\item $FE.Dec(sk_f, c)$
	\begin{itemize}
	\item We want to be able to evaluate $U(f,x)$ and we have access to $\hat{U}_x$ 
		so we need to recover the correct labels to describe $f$.
	\item The cipher contains $\hat{U}_x$ and the $c_{i,b} =  Enc(pk_{i,b}, k_{i,b})$ for both $b = 0$ and $b = 1$.
		In $sk_f$ we have the associated private keys (only for the bits of $f$, not for the others). So for every
		$c_{i,b}$ we will attempt to compute $Dec(sk_{i,f_i}, c_{i,b})$ for both $b = 0$ and $b = 1$. We assume that if we try to
		decrypt a cipher with the wrong key the output will be $\perp$ so we can detect which key works for every input pin.
		The other value will decrypt properly and now we have, for each $i$ a label $k_{i,f_i}$ that we can feed in $\hat{U}_x$
		and finally output $f(x)$.
	\end{itemize}
\end{itemize}
	
The protocol is trivially correct if the underlying encryption scheme and garbling scheme are correct.

\subsection{}
Now we will construct a simulator $S$ to show that the scheme is secure according to the real/ideal--world definition.
We will use the $Sim$ that comes from Yao's garbling described in Part~\ref{p4a} as a building block. Here is how
$S(mpk, f, f(x), sk_f)$ works:

\begin{enumerate}
\item Run $Sim(f(x),  \text{ description of }  U(\cdot, x))$ and get $(\bar{U}_x, \bar{k}_{i,f_i})$ that are 
computationally indistinguishable from the true garbling $(\hat{U}_x, k_{i,f_i})$.
\item Encrypt as before but using  $(\bar{U}_x, \bar{k}_{i,b})$ for $b = f_i$ and a random string for the other value of $b$.
\end{enumerate}
Provided the garbling is secure and so is the public encryption scheme then the simulator's view is indistinguishable from the
view provided by the true scheme and the security of the Functional Encryption scheme follows.





